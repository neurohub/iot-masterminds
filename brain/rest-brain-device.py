#!/usr/bin/env python

"""
	The device only test data against trained models

	Available Endpoints:

	- POST /test/{modelName}

"""

import json
import web
import ast
from brain import Brain

IP = '0.0.0.0'
PORT = 8081

paths = (
	'/test/(.*)', 'BrainTest',
)

# app = web.application(paths, globals())

class RESTAPI(web.application):
    def run(self, *middleware):
        func = self.wsgifunc(*middleware)
        return web.httpserver.runsimple(func, (IP, PORT))


def notfound():
	structure = brain.getStructure();
	message = "The current model supports " + str(structure[0]) + " inputs and " + str(structure[1])+ " outputs. "
	endpoints = [
			{'type': 'POST', 'url': '/test'}
		]
	response = json.dumps({'message':message,'endpoints':endpoints})
	web.header('Content-Type', 'application/json')
	return web.notfound(response)


def internalerror():
	return web.internalerror("Internal error.")


## POST { className: "className", inputs: [1,2,3]  }
class BrainTest:
	def POST(self, className):
		try:
			brain = Brain.load(className)
			web.header('Content-Type', 'application/json')
			data = ast.literal_eval(web.data())

			print className
			print data
			response = json.dumps(brain.test(data))
			return response

		except Exception as e:
			return e

if __name__ == "__main__":
	app = RESTAPI(paths, globals())
	app.notfound = notfound
	app.internalerror = internalerror
	app.run()
