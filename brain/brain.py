#!/usr/bin/python

from pybrain.tools.shortcuts import buildNetwork
from pybrain.datasets import SupervisedDataSet
from pybrain.supervised.trainers import BackpropTrainer
import random
import numpy
import pickle
import time
import glob

class Brain():
	def __init__(self, inputLayserSize, outputLayserSize):
		self.inputLayserSize = inputLayserSize
		self.outputLayserSize = outputLayserSize
		self.net = buildNetwork(inputLayserSize, inputLayserSize + 1 , outputLayserSize)
		self.ds = SupervisedDataSet(inputLayserSize, outputLayserSize)
		self.sampleSize = 0
	
	def getStructure(self):
		return [self.inputLayserSize, self.outputLayserSize]

	def addKnowledge(self, inputs, outputs):
		if(len(inputs)!=self.inputLayserSize):
			raise Exception("Input array should contain " + str(self.inputLayserSize) + " elements!")

		if(len(outputs)!=self.outputLayserSize):
			raise Exception("Output array should contain " + str(self.outputLayserSize) + " elements!")

		# Add extra noisy data
		for i in range(10):
			r = (numpy.random.random(numpy.size(inputs))-.5)/100
			inputs = (numpy.array(inputs) + r).tolist()			
			self.ds.appendLinked(inputs, outputs)

		self.sampleSize += 1

	def train(self):
		start_time = time.time()
		trainer = BackpropTrainer(self.net, self.ds)
		errors = trainer.trainUntilConvergence()
		return errors
	
	def test(self, inputs):
		start_time = time.time()
		if(len(inputs)!=self.inputLayserSize):
			raise Exception("Input array should contain " + str(self.inputLayserSize) + " elements!")
		prediction = self.net.activate(inputs)
		return prediction.tolist()
		
	def save(self, className):
		filename = className + time.strftime(".%Y%m%d%H%M%S.nn")
		pickle.dump( self, open(filename, "wb" ))
		return filename
	
	def getSampleSize(self):
		return self.sampleSize

	@staticmethod
	def load(className):
		files = glob.glob('./' + className + ".*.nn")
		filename = files[-1]
		return pickle.load( open(filename, "rb" ))

def trainGates():
	brain = Brain(2,3);
	print 'addKnowledge'
	brain.addKnowledge([0,0], [0,0,0])
	brain.addKnowledge([0,1], [1,1,0])
	brain.addKnowledge([1,0], [1,1,0])
	brain.addKnowledge([1,1], [0,1,1])
	print 'train'
	brain.train()
	brain.save('model')

def testGates():
	brain = Brain.load('model')
	print 'test'
	print brain.test([0,0])
	print brain.test([0,1])
	print brain.test([1,0])
	print brain.test([1,1])


if __name__ == '__main__':
	trainGates()
	testGates()

