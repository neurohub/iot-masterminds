## Install Python dependencies 

```Python
sudo pip install web.py
sudo pip install pybrain
sudo pip install numpy
```

## Training the model : 
## Python script example
```Python
from brain import Brain
brain = Brain(2,3);
brain.addKnowledge([0,0], [0,0,0])
brain.addKnowledge([0,1], [1,1,0])
brain.addKnowledge([1,0], [1,1,0])
brain.addKnowledge([1,1], [0,1,1])
brain.train()
brain.save('modelName')
```

## testing data 
## Python script example
```Python
from brain import Brain
brain = Brain.load('modelName')
print brain.test([0,0])
print brain.test([0,1])
print brain.test([1,0])
print brain.test([1,1])
```

## Using the API to test data
```bash
>> python rest-brain-device.py
 http://0.0.0.0:8080/
```

POST json formated input array to http://localhost:8080/test/{modelName}

*body:*
[0,0]
*response*
[
  0.004066188951280747,
  0.003345905196286081,
  -0.0008623076783722539
]
