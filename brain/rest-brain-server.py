#!/usr/bin/env python

"""
	The server only trains models

	Available Endpoints:

	- POST /add/{modelName}
	- POST /train/{modelName}
	- GET /download/{modelName} 

"""

import json
import web
import ast
import glob
from brain import Brain

IP = '0.0.0.0'
PORT = 8080

paths = (
	'/add/(.*)', 'BrainAdd',
	'/train/(.*)', 'BrainTrain',	
	'/download/(.*)', 'BrainDownload'
)

brains = {}

class RESTAPI(web.application):
    def run(self, *middleware):
        func = self.wsgifunc(*middleware)
        return web.httpserver.runsimple(func, (IP, PORT))


def notfound():
	endpoints = [
			{'type': 'POST', 'url': '/add/{modelName}'},
			{'type': 'POST', 'url': '/train/{modelName}'},
			{'type': 'GET', 'url': '/download/{modelName}'}
		]
	response = json.dumps({'endpoints':endpoints})
	web.header('Content-Type', 'application/json')
	web.header('Access-Control-Allow-Origin', '*')
	web.header('Access-Control-Allow-Headers', 'x-requested-with, Content-Type, origin, authorization, accept, client-security-token')
	return web.notfound(response)


def internalerror():
	return web.internalerror("Internal error.")


class BrainAdd:
	def POST(self, className):
		try:
			data = json.loads(web.data())

			inputs = data['inputs']
			outputs = data['outputs']

			if className in brains:
				brain = brains[className]
				message = "Data added to temporary model " + className 				
			else:
				brain = Brain(len(inputs),len(outputs))
				brains[className] = brain
				message = "temporary model " + className + " created"

			brains[className].addKnowledge(inputs,outputs)
			response = json.dumps({'message':message, "sampleSize": brains[className].getSampleSize()})
			web.header('Content-Type', 'application/json')
			web.header('Access-Control-Allow-Origin', '*')
			web.header('Access-Control-Allow-Headers', 'x-requested-with, Content-Type, origin, authorization, accept, client-security-token')
			return response

		except Exception as e:
			return e

# Trains the model with  the data added on previous calls
# save the trained model as file
# the file is called className.{datetime}.nn
class BrainTrain:
	def POST(self, className):
		try:
			if className not in brains:
				return className + " is not a model"

			brains[className].train()
			brains[className].save(className)

			message = "Model " + className + " is now trained!"
			response = json.dumps({'message':message, "sampleSize": brains[className].getSampleSize()})
			
			## Since the Model is already saved into a file, 
			## all training data for this className is deleted from Memory
			del brains[className]

			web.header('Content-Type', 'application/json')
			web.header('Access-Control-Allow-Origin', '*')
			web.header('Access-Control-Allow-Headers', 'x-requested-with, Content-Type, origin, authorization, accept, client-security-token')
		
			return response

		except Exception as e:
			return e

class BrainDownload:
	def GET(self, className):
		try:
			# get the model file 
			files = glob.glob('./' + className + ".*.nn")
			file = files[-1]
			f = open(file, 'r')
			filecontent = f.read()
			f.close()
			return filecontent
		except Exception as e:
			return e


if __name__ == "__main__":
	app = RESTAPI(paths, globals())
	app.notfound = notfound
	app.internalerror = internalerror
	app.run()
