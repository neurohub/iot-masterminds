// Load Grove module
var groveSensor = require('jsupm_grove');
// Create the Grove LED object using GPIO pin 2
var led = new groveSensor.GroveLed(2);
// Print the name
console.log(led.name());
// Turn the LED on and off 10 times, pausing one second
// between transitions
var i = 0;
var waiting = setInterval(function() {
        if ( i % 2 == 0 ) {
            led.on();
        } else {
            led.off();
        }
        i++;
        if ( i == 20 ) clearInterval(waiting);
        }, 1000);
