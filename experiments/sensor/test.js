//Load the libraries

// Constants
var DEVICE_NAME = 'Nabucodonosor';
var DEVICE_TYPE = 'EdisonLED';

//Log Startup on the client
console.log('Mini-minion starting:');

var groveSensor = require('jsupm_grove');
var restify     = require('restify');
var led         = new groveSensor.GroveLed(2);

// create a http server
var server = restify.createServer();

// use a body parser, needed for post request with body
server.use(restify.bodyParser());

// Set the route with callback function method GET and POST
server.get('/gpio/:id', getValue);
server.post('/gpio/:id', setValue);
server.listen(8080, function() {
    console.log(
        'Mini-minion %s listening at %s', 
        server.name,
        server.url
    );
});

function getValue(req, res, next)
{
    //Log REST Get value
    console.log('MinionGet-');

   // check if all parameters are present
    if (req.params.id === undefined || req.params.value === undefined) {
        res.json({error: "missing value"});
    }else{
        // connect the mraa lib to the GPIO needed
        var currentPin = new mraa.Gpio(req.params.id);

        // set it in input to read data
        currentPin.dir(mraa.DIR_IN);

        // send response with reading the GPIO
        res.json({
           "device":{
              "name":DEVICE_NAME,
              "type":DEVICE_TYPE,
              "sensor":{
                 "potentiometer":27
              },
              "state":{
                id: req.params.id,
                value : currentPin.read(),
              }
           }
        });

    }
    next();
}

function setValue(req, res, next)
{
    //Log REST Get value
    console.log('MinionSet-');

    // check if all parameters are present
    if(req.params.id === undefined || req.params.value === undefined){
        res.json({error: "missing value"});
    }
    else{
        switch (req.params.id)
        {
           case "led":
              if (req.params.value === 'on')
              {
                led.on();
                console.log('on');
              }
              else
              {
                console.log('off');
                led.off();
              }
              break;
        }
        // send response to client
        //  res.json({value: req.params.value,id: req.params.id, dir: req.params.value});
        res.json({
          device:{
            name:DEVICE_NAME,
            type:DEVICE_TYPE,
            sensor:{
              potentiometer:27
            },
            state:{
              id : req.params.id,
              value : req.params.value
            }
          }
        });
      }
      return next();
}
