//Load the libraries

//Log Startup on the client
console.log('Minion starting:'); 

var mraa = require('mraa');
var groveSensor = require('jsupm_grove');
var restify = require('restify');
var fs = require('fs');
var led = new groveSensor.GroveLed(2);
var ledState = 0;
var button = new groveSensor.GroveButton(3);
var rotary = new groveSensor.GroveRotary(0);
var DEVICE_NAME = 'Nabucodonosor';
var DEVICE_TYPE = 'EdisonLED';
var request = require('request');

// instantiate rest server and sockets server
var server = restify.createServer();

// add in json handler
server.use(restify.jsonp());

//////////////////////////
// setup websocket handlers
var socketio = require('socket.io');
var io = socketio.listen(server.server);

//server.get(/data/, io.handle);
//server.post(/data/, io.handle);

// map websocket interfaces
// io.set('transports', [ 'websocket' , 'flashsocket' ]);

io.sockets.on('connection', function(socket) {  
    console.log('Socket connected...');
    
    var interval = setInterval(function() {
        console.log("Sending")
	socket.emit({
            "device":{
                "name":DEVICE_NAME,
                "type":DEVICE_TYPE,
                "sensor":{
                    "potentiometer":rotary.abs_value(),
		    "button":button.value()
                },
                "state":{
                    "id": req.params.id,
                    "value" : ledState,
                }
            }
        },500);
    });
        
    socket.on('disconnect',function(){
	clearInterval(interval);
        socket.emit('message', 'Stopping Stream');
	console.log('Server has disconnected');
    });
});

app = server.listen(8080, function() {
    console.log('Minion %s listening at %s', server.name, server.url);
});

///////////////////////////////////////////////////////////////////////////////
// 
// use a body parser, needed for post request with body
server.use(restify.bodyParser());
server.use(restify.queryParser());

// map static client into restify router with / route
server.get(/\/css\/.*/,restify.serveStatic({
    directory: '../../client/',
    default: 'style.css'
}));

server.get(/\/js\/.*/,restify.serveStatic({
    directory: '../../client/',
    default: 'style.css'
}));

server.get(/\/bower_components\/.*/,restify.serveStatic({
    directory: '../../client/',
    default: 'style.css'
}));

server.get(/\/services\/.*/,restify.serveStatic({
    directory: '../../client/',
    default: 'style.css'
}));

server.get(/^\/$/, restify.serveStatic({
    directory: '../../client/',
    default: 'index.html'
}));

///////////////////////////////////////////////////////////////////////////////
// Rest functions.
// Set the route for GPIO with callback function method GET and POST
server.get('/gpio/:id', getValue);
server.post('/gpio/:id', setValue);

// Get Master
// return master 'trainer' in system
server.get(/master/, function yesMaster(req, res, next) {
    fs.readFile(__dirname + '/master.json', function (err, data) {
        if (err) {
            next(err);
            return;
        }
        
        res.setHeader('Content-Type', 'text/json');
        res.writeHead(200);
        res.end(data);
        next();
    });
});

// Get Master
// return master 'trainer' in system
server.get(/minions/, function minions(req, res, next) {
    fs.readFile(__dirname + '/minions.json', function (err, data) {
        if (err) {
            next(err);
            return;
        }
        
        res.setHeader('Content-Type', 'text/json');
        res.writeHead(200);
        res.end(data);
        next();
    });
});


// Get status
// miniontype 
server.get(/type/, function minionstype(req, res, next) {
    fs.readFile(__dirname + '/miniontype.json', function (err, data) {
        if (err) {
            next(err);
            return;
        }
        
        res.setHeader('Content-Type', 'text/json');
        res.writeHead(200);
        res.end(data);
        next();
    });
});


// refresh training
// model to pass to the python stack

// Set the route for GPIO with callback function method GET and POST
server.get(/startstop/, startLoop);
server.post(/startstop/, stopLoop);

function startLoop(req, res, next)
{
    //Log REST Get value
    console.log('LoopStart-'); 
    var rotary_on = 0;
    if (rotary.abs_value() > 500)
    {
	rotary_on = 1;
    }
    
    res.json({status: 'running'});
    request.post(
        'http://10.10.107.87:8081/test/model',
        {json:[rotary_on,button.value()]
        },
        function (error, response, body) {
            if (!error && response.statusCode == 200) {
                console.log(body)
            }
        }
    );    
    
    next();
}

function stopLoop(req, res, next)
{
    //Log REST Get value
    console.log('LoopStop-'); 
    
    // send response with reading the GPIO
    res.json({status: 'stopped'});
    next();
}

//////////////////////////////////////////////////////////////////////////////
//

function getValue(req, res, next)
{
    //Log REST Get value
    console.log('MinionGet-'); 
    
    // check if all parameters are present
    if(req.params.id === undefined){
        res.json({error: "missing value"});
    }
    else{
	console.log(req.params.id);
	if (req.params.id === 'led')
	{
	    // send response with reading the GPIO
            res.json({
                "device":{
                    "name":DEVICE_NAME,
                    "type":DEVICE_TYPE,
                    "sensor":{
                        "potentiometer":rotary.abs_value(),
		        "button":button.value()
                    },
                    "state":{
                        "id": req.params.id,
                        "value" : ledState,
                    }
                }
            });
	}
    }
    next();
}

function setValue(req, res, next)
{
    //Log REST Get value
    console.log('MinionSet-'); 
    
    // check if all parameters are present
    if(req.params.id === undefined || req.params.value === undefined){
        res.json({error: "missing value"});
    }
    else{
        
        switch (req.params.id)
        {
            case "led":
            if (req.params.value === 'on')
            {
                led.on();
                console.log('on');
		ledState = 1;
            }
            else
            {
                console.log('off');
                led.off();
		ledState = 0;
            }
            break;
        }
        // send response to client
        // send response to client
        //  res.json({value: req.params.value,id: req.params.id, dir: 
        req.param$
        res.json({
            device:{
                name:DEVICE_NAME,
                type:DEVICE_TYPE,
                sensor:{
                    "potentiometer":rotary.abs_value(),
	            "button":button.value()
                },
                state:{
                    "id" : req.params.id,
                    "value" : req.params.value
                }
            }
        });
    }
    return next();
}

///////////////////////////////////////////////////////////////////////////////////
// socket.io listeners

