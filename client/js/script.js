var app = angular.module('IoTApp',
    ['nvd3', 'services-app', 'brainService', 'ngResource', 'checkboxGroupApp', 'GraphApp']);

app.config(['$httpProvider', function($httpProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
    }
]);

//Controller 
app.controller('MainCtrl', function ($scope, train, DevicesAvailable, DeviceInformation) {

    $scope.DeviceList = DevicesAvailable;

    $scope.DeviceInformation = DeviceInformation;

    // // get 
    $scope.Devices = [

        { id: 1, name: 'device1', run: true, description: [{
            type: 'Analog',
            name: 'Temperature',
        },
        ]
         },
        { id: 2, name: 'device2', run: true, description: [{
            type: 'Digital',
            name: 'Button',
        },
        ] },
        { id: 3, name: 'device3', run: true, description: [{
            type: 'Analog',
            name: 'Potentiometers',
        },
        {
            type: 'Digital',
            name: 'Presence',
        }
        ] },
        { id: 4, name: 'device4', run: true, description: [{
            type: 'Analog',
            name: 'Temperature Sensor',
        },
        ] }
    ];

    $scope.DevicesSelected = [];

    $scope.checkAll = function () {
        $scope.DevicesSelected = angular.copy($scope.Devices);
    };
    $scope.uncheckAll = function () {
        $scope.DevicesSelected = [];
    };

    train($scope.Devices, 'testModel');
});

