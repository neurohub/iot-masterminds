﻿angular.module('GraphApp', ['nvd3', 'services-app'])
.controller('GraphController', ['$scope', function ($scope) {

}])
.directive('sensorgraph', function () {

    var plot_data = { x: [], y: [] }
    var number_of_samples = 20;

    for (var i = 0; i < number_of_samples; i++){
        plot_data.x.push(i)
        plot_data.y.push(Math.random())
    }

    return {
        require: 'ngModel',
        restrict: 'AE',
        scope: {
            item: '=ngModel'
        },
        template: '<div ng-init="init(item)"><button ng-click="SendInfo(item)">GetInfo</button>' +
            '<nvd3 options="options" data="data"></nvd3>' +
            '<div>{{infoSelected|json}}</div></div>',
        controller: ['$scope', function($scope) {
            $scope.options = {
                chart: {
                    type: 'lineChart',
                    height: 180,
                    margin: {
                        top: 20,
                        right: 20,
                        bottom: 40,
                        left: 55
                    },
                    x: function(d) { return d.x; },
                    y: function(d) { return d.y; },
                    useInteractiveGuideline: true,
                    duration: 500,
                    yAxis: {
                        tickFormat: function(d) {
                            return d3.format('.01f')(d);
                        }
                    }
                }
            };

            $scope.options.chart.duration = 0;
            $scope.options.chart.yDomain = [-1, 1];

            $scope.data = [{ values: [], key: '' }];

            var init = function(item) {
                $scope.data = [{ values: [], key: item.name }];
            }

            var x = 0;

            $scope.SendInfo = function(item) {
                $scope.infoSelected = item;
                console.info(item);
                $scope.assignSnapshot(item);
            };

            $scope.options.chart.duration = 0;
            $scope.options.chart.yDomain = [-1, 1];

            $scope.infoSelected = "";

            setInterval(function() {
                x = plot_data.x[number_of_samples - 1] + 1;

                plot_data.x.push(x);
                plot_data.x.shift();

                plot_data.y.push(Math.random());
                plot_data.y.shift();

                $scope.data[0].values.push({ x: plot_data.x[number_of_samples - 1], y: plot_data.y[number_of_samples - 1].toFixed(2) });
                if ($scope.data[0].values.length > 20)
                    $scope.data[0].values.shift();

                $scope.$apply(); // update chart
            }, 500);

            $scope.assignSnapshot = function (item) {
                var _data = angular.copy(plot_data);
                item.snapshots.push(_data);
            }
        }]
    };
});