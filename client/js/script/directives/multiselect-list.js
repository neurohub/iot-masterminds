﻿
var app = angular.module('checkboxGroupApp', ['ngResource']);

app.directive("checkboxGroup", function () {
    return {
        restrict: "A",
        link: function (scope, elem, attrs) {
            // Determine initial checked boxes
            if (scope.DevicesSelected.indexOf(scope.item) !== -1) {
                elem[0].checked = true;
            }

            // Update array on click
            elem.bind('click', function () {
                var index = scope.DevicesSelected.indexOf(scope.item);
                console.info(index);
                // Add if checked
                if (elem[0].checked) {
                    if (index === -1) scope.DevicesSelected.push(scope.item);
                }
                    // Remove if unchecked
                else {
                    if (index !== -1) scope.DevicesSelected.splice(index, 1);
                }
                // Sort and update DOM display
                scope.$apply(scope.DevicesSelected.sort(function (a, b) {
                    return a - b;
                }));
            });
        }
    }
});
