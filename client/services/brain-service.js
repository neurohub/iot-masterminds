angular.module('brainService', [])

.factory('train', ['$http', function($http) {

  TRAINING_SERVER = 'http://localhost:8080'
   return function(data, className) {
      console.log(data);

      $http(
        {
          method: 'POST', 
          url: TRAINING_SERVER+'/add/'+className,
          data: data,
          headers:{
                'Content-Type': 'application/json' , 
                'Access-Control-Allow-Origin': '*'
          }})
        .success(function(d){ console.log( "yay" ); })
        .error(function(d){ console.log( "nope" ); });

   };
 }]);
